<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
<!--Case Studies-->
 <div class="bluebgsection section-padding" id="caseStudies">
    	<div class="container common-container-xl">
		    		<div class="row align-items-center">
			    			<div class="col-7">
			    				<h2>Related Case Studies</h2>
			    			</div>
			    			<div class="col-5 slider-nav text-right"></div>
		    		</div>    
    		</div>	
    		<div class="case-studies-container">
						<div class="case-studies-block">
							<?php 
								$args = array(
							    'post_type' => 'casestudies',
							    'orderby' => 'date',
							    'order' => 'ASC',
							    'posts_per_page' => -1
							     );
							$query = new WP_Query( $args ); 
							while ($query->have_posts()) {
							    $query->the_post();
							    $post_title = get_the_title();
							    $post_id = get_the_id();
							    $post_url = get_permalink($post_id);
							    ?>
							    	<div>
							    		<a href="<?php echo $post_url?>"> 
							    			<div class="cs-thumb"><?php the_post_thumbnail('related-case-study');?></div>
							    			<div class="cs-content">
							    				<h3><?php echo $post_title ?></h3> 
							    				<?php the_excerpt(); ?>
							    			</div>
							    		</a>
							    	</div>
							   <?php

							}
							wp_reset_query();
							?>
						</div>
					</div>
    	</div>
    </div>
<!--/Case Stdies-->
<!--Footer Top Content-->
<div class="lightbluebg section-padding" id="talkToExperts">
	 <div class="orbit left-bottom_half noanimation"></div>
	<div class="container common-container-lg text-center">
		<h4>Optimize your operational expenditure with AOne</h4>
		<p>Simple deployment and asset integrations,<br /> check out the features, or get in touch.</p>
		<p><a href="javascript:void(0);">Talk to our experts</a></p>
	</div>
	 <div class="orbit right-top_half noanimation"></div>
</div>
<!--/Footer-Top-Content-->
			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- #content -->
   
	<footer id="colophon" class="site-footer">
<div class="container common-container-xl">
		<?php if ( has_nav_menu( 'footer' ) ) : ?>
			<nav aria-label="<?php esc_attr_e( 'Secondary menu', 'twentytwentyone' ); ?>" class="footer-navigation">
				<ul>
                   <li class="featureshead">Features:</li>
                    <?php 
                        $args = array(
                          'orderby' => 'name',
                          'hierarchical' => 1,
                          'taxonomy' => 'category',
                          'hide_empty' => 0,
                          'parent' => 0,
                          //'exclude' => '1,179, 2127',
                        );
                        $categories = get_categories($args);

                        foreach($categories as $category) {

                          echo '<li><a href="' . get_category_link($category->cat_ID) . '" title="' . $category->name . '">' . $category->name . '</a></li>';

                          } 
                      ?>
                </ul>
				<ul class="footer-navigation-wrapper">
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'footer',
							'items_wrap'     => '%3$s',
							'container'      => false,
							'depth'          => 1,
							//'link_before'    => '<span>',
							//'link_after'     => '</span>',
							'fallback_cb'    => false,
						)
					);
					?>
				</ul><!-- .footer-navigation-wrapper -->
			</nav><!-- .footer-navigation -->
		<?php endif; ?>
		<div class="site-info">
			<div class="row">
				<div class="col-10">
					<div class="row">
						<ul class="col-3">
							<li>Solutions</li>
							<?php
								$args = array(
								    'post_type' => 'post',
								    'post_status' => 'publish',
								    'category_name' => 'Solutions',
								    'posts_per_page' => -1,
								);
								$arr_posts = new WP_Query( $args );
								  
								if ( $arr_posts->have_posts() ) :
								  
								    while ( $arr_posts->have_posts() ) :
								        $arr_posts->the_post();
								        ?>
								            <li>
								                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								            </li>
								        <?php
								    endwhile;
								endif;
							?>
						</ul>
						<ul class="col-3">
							<li>Industries</li>
							<?php
								$args = array(
								    'post_type' => 'post',
								    'post_status' => 'publish',
								    'category_name' => 'Industries',
								    'posts_per_page' => -1,
								);
								$arr_posts = new WP_Query( $args );
								  
								if ( $arr_posts->have_posts() ) :
								  
								    while ( $arr_posts->have_posts() ) :
								        $arr_posts->the_post();
								        ?>
								            <li>
								                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								            </li>
								        <?php
								    endwhile;
								endif;
							?>
						</ul>
						<ul class="col-3">
							<li>RESOURCES</li>
							<?php
								$args = array(
								    'post_type' => 'post',
								    'post_status' => 'publish',
								    'category_name' => 'Solutions',
								    'posts_per_page' => 5,
								);
								$arr_posts = new WP_Query( $args );
								  
								if ( $arr_posts->have_posts() ) :
								  
								    while ( $arr_posts->have_posts() ) :
								        $arr_posts->the_post();
								        ?>
								            <li>
								                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								            </li>
								        <?php
								    endwhile;
								endif;
							?>
						</ul>
						<ul class="col-3">
							<li>Company</li>
							<?php
								$args = array(
								    'post_type' => 'post',
								    'post_status' => 'publish',
								    'category_name' => 'Solutions',
								    'posts_per_page' => -1,
								);
								$arr_posts = new WP_Query( $args );
								  
								if ( $arr_posts->have_posts() ) :
								  
								    while ( $arr_posts->have_posts() ) :
								        $arr_posts->the_post();
								        ?>
								            <li>
								                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								            </li>
								        <?php
								    endwhile;
								endif;
							?>
						</ul>
					</div>
				</div>
				<div class="col-2">
					<div class="site-name">
						<?php if ( has_custom_logo() ) : ?>
							<div class="site-logo"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/Atsuya-logo-white.png" alt="Atsuya" /></div>
						<?php else : ?>
							<?php if ( get_bloginfo( 'name' ) && get_theme_mod( 'display_title_and_tagline', true ) ) : ?>
								<?php if ( is_front_page() && ! is_paged() ) : ?>
									<?php bloginfo( 'name' ); ?>
								<?php else : ?>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>
								<?php endif; ?>
							<?php endif; ?>
						<?php endif; ?>
					</div><!-- .site-name -->
					<?php get_template_part( 'template-parts/footer/footer-widgets' ); ?>
				</div>
			</div>
			
			
			<?php
			if ( function_exists( 'the_privacy_policy_link' ) ) {
				the_privacy_policy_link( '<div class="privacy-policy">', '</div>' );
			}
			?>

			<div class="powered-by row">
				<div class="col-12 pad0 mtop1">
				<p>&copy; 2021-22 Atsuya Technologies. All Rights Reserved.</p>
				</div>
			</div><!-- .powered-by -->

		</div><!-- .site-info -->
		</div><!--Common Container-->
	</footer><!-- #colophon -->
</div><!-- #page -->

<!--FREE-DEMO-LINK-Modal-->
<div class="demo-modal">
	 <h4 class="position-relative">Request for a Demo <span class="closemodal pull-right">&#x2715;</span></h4>
	 <div class="demo-modal_content">
	 		<?php echo do_shortcode('[contact-form-7 id="219" title="Free Demo Form"]'); ?>
	 </div>
</div>
<!--/END-FREE-DEMO-Modal-->

<?php wp_footer(); ?>
<script>
jQuery('.case-studies-block').slick({
	  dots: false,
	  arrows:true,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 1,
  	slidesToScroll: 1,
	  centerMode: false,
	  variableWidth: true,
	  prevArrow: '<button class="slick-prev back-arrow slick-arrow slick-disabled" aria-label="Previous" type="button" aria-disabled="true" style=""><svg class="svg-icon" width="24" height="24" aria-hidden="true" role="img" focusable="false" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M20 13v-2H8l4-4-1-2-7 7 7 7 1-2-4-4z" fill="currentColor"></path></svg></button>',
	  nextArrow:'<button class="slick-next next-arrow slick-arrow" aria-label="Next" type="button" style="" aria-disabled="false"><svg class="svg-icon" width="24" height="24" aria-hidden="true" role="img" focusable="false" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="m4 13v-2h12l-4-4 1-2 7 7-7 7-1-2 4-4z" fill="currentColor"></path></svg></button>',
	  appendArrows: '.slider-nav',
	  adaptiveHeight: true
});
jQuery('.demobtn').on('click', function(){
	jQuery('body').append('<div class="modal-backdrop"></div>');
	jQuery('.demo-modal').addClass('showmodal');
})

jQuery('.closemodal').on('click', function(){
	jQuery('.demo-modal').removeClass('showmodal');
	jQuery('.modal-backdrop').remove();
})

<?php if( is_front_page() ) { ?>
jQuery('.partners-section').slick({
	slidesToShow: 7,
  slidesToScroll: 7,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 1000,
  arrows: false,
  dots:false,
  cssEase: 'linear'
});
jQuery('#partners ul').slick({
	slidesToShow: 5,
  slidesToScroll: 1,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 1000,
  arrows: false,
  dots:false,
  cssEase: 'linear'
});
<?php } ?>
</script>
</body>
</html>
