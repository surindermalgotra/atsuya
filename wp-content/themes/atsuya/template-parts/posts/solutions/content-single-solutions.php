<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php if (has_post_thumbnail( $post->ID ) ): ?>
  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
<?php endif; ?>
	
	<header class="entry-header alignwide">
		<div class="container-fluid bannerbg pad0" style="background-image:url(<?php echo $image[0] ?>); background-repeat:no-repeat;background-position:right center;background-color:#E7F2FF">
			<div class="container common-container-xl banner-content"><?php the_title('<h1 class="entry-title">', '</h1>'); ?><p class="breadcrumbs"><?php the_breadcrumb(); ?></p></div>
		</div>
	</header><!-- .entry-header -->
<div class="container common-container-xl">
	<div class="entry-content mtop5">
		<?php
		    $solutionPostTopSection = get_field('solutions_posts_top_section');
		    if($solutionPostTopSection){
			$topsectiontitle = $solutionPostTopSection['top_section_title'];
			$topSectionThumb = $solutionPostTopSection['top_section_thumb'];
			$topSectionContent= $solutionPostTopSection['top_section_content'];
			}
		?>
        <div class="row vjustify-content">
        	<div class="col-sm-4">
        		<img src="<?php echo $topSectionThumb ?>" alt="" title="" />
        	</div>
        	<div class="col-sm-8">
        		<h3><?php echo $topsectiontitle ?></h3>
        		<p><?php echo $topSectionContent ?></p>
        	</div>
        </div>

		<?php /*--
		the_content();

		wp_link_pages(
			array(
				'before'   => '<nav class="page-links" aria-label="' . esc_attr__( 'Page', 'twentytwentyone' ) . '">',
				'after'    => '</nav>',
				
				'pagelink' => esc_html__( 'Page %', 'twentytwentyone' ),
			)
		);
		--*/ ?>
	</div><!-- .entry-content -->
</div>
<!--Our Solutions Section-->
<div class="container-fluid lightbluebg section-padding mtop5">
	<?php
		$ourSolutionSection = get_field('our_solutions_section');
		if( $ourSolutionSection ): 
               $ourSolutionsTitle = $ourSolutionSection['our_solutions_title'];
			   $ourSolutionsescription = $ourSolutionSection['description'];
			   $ourSolutionsThumb = $ourSolutionSection['our_solutions_thumb'];
		?>
		    <div class="container common-container-xl">
		<div class="row">
			<div class="col-sm-8">
				<h3><?php echo $ourSolutionsTitle ?></h3>
				<?php if($ourSolutionsescription) { ?>
					<p><?php echo $ourSolutionsescription ?></p>
				<?php } ?>
				<ul class="our-solution__list">
					<?php 
						 $oursolutionsgroup = get_field('our_solutions_section');
					     $repeaterLists = $oursolutionsgroup['our_solutions_lists'];
					     foreach($repeaterLists as $repeaterList) {?>
					     	<li>
					     		<span class="list-point_icon"><img src="<?php echo $repeaterList["list_icon"] ?>" alt="" title="" /></span> 
					     		<span><?php echo $repeaterList["list_point_content"] ?></span></li>
					      <?php }
					?>
				</ul>
			</div>
			<div class="col-sm-4 text-right">
				<img src="<?php echo $ourSolutionsThumb ?>" alt="" title="" />
			</div>
		</div>
	</div>
		<?php endif; ?>
</div>
<!--/end Our-solutions-section-->
<?php
  $ourimpactgroup = get_field('our_imapct_section');
  if($ourimpactgroup) { ?>
<div class="container-fluid section-padding secondarybluebg">
	<div class="container common-container-lg">
		<h3 class="text-center text-white">Our Impact</h3>
		<ul class="impact-list mt-4">
			<?php 
				 $sdgIcon =  $ourimpactgroup['impact_sdg_logo'];
			     $ourimpactrepeater = $ourimpactgroup['impact_points'];
			     foreach($ourimpactrepeater as $impactList) {?>
			     	<li>
			     		<span class="impact-icon"><img src="<?php echo $impactList["impact_point_icon"] ?>" alt="" title="" /></span> 
			     		<?php echo $impactList["impact_point_description"] ?></li>
			      <?php }?>
			       <li><img src="<?php echo $sdgIcon ?>" alt="" title="" /></li>
		</ul>
	</div>
</div>
<?php } ?>
</article><!-- #post-<?php the_ID(); ?> -->
