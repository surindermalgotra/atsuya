<?php
/**
  Developer:   Sumesh Kumar Sharma
  Date:		   05-11-2022
* Enqueues child theme stylesheet, loading first the parent theme stylesheet.
*/
function wpdocs_theme_name_scripts() {
    //wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
    wp_dequeue_style( 'twenty-twenty-one-style' );
    wp_enqueue_style('bootstrap-css', '//cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css');
    wp_enqueue_style('atsuya-style', get_stylesheet_uri(), array(), wp_get_theme()->get('Version'));
    wp_enqueue_script('bootstrap', '//cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js', array('jquery'), '1.0', true );
    wp_enqueue_style('slick-css', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');
    wp_enqueue_script('slick-js', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array('jquery'),'1.1', true);
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts', 11 );

//Remove parent theme fonts
function wpse_dequeue_google_fonts() {
  //wp_dequeue_style( 'twentyseventeenone-fonts' );
  //wp_deregister_style( 'dashicons' ); 
  wp_dequeue_script('thickbox');
  wp_deregister_style('thickbox');
}
add_action( 'wp_enqueue_scripts', 'wpse_dequeue_google_fonts', 20 );

// REGISTER NAVIGATION MENUS
  register_nav_menus( array(
    'footer_bottom_area'  => __('Footer Bottom Area','twentyseventeen'),
        'footer_main_pages'  => __('Footer Pages Nav','twentyseventeen'),
    ));
function hide_admin_bar_from_front_end(){
  if (is_blog_admin()) {
    return true;
  }
  return false;
}
add_filter( 'show_admin_bar', 'hide_admin_bar_from_front_end' );

/*--Custom post type for Case Studies--*/
function my_custom_post_casestudies() {
  $labels = array(
    'name'               => _x( 'Case Studies', 'post type general name' ),
    'singular_name'      => _x( 'Case Study', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'Case Study' ),
    'add_new_item'       => __( 'Add New Case Study' ),
    'edit_item'          => __( 'Edit Case Study' ),
    'new_item'           => __( 'New Case Study' ),
    'all_items'          => __( 'All Case Study' ),
    'view_item'          => __( 'View Case Study' ),
    'search_items'       => __( 'Search Case Study' ),
    'not_found'          => __( 'No Case Study found' ),
    'not_found_in_trash' => __( 'No Case Study found in the Trash' ), 
    //'parent_item_colon'  => ,
    'menu_name'          => 'Case Studies'
  );
  $args = array(
    'labels'        => $labels,
      'supports'            => array( 'title', 'editor', 'thumbnail', 'tags', 'excerpt'),
    'hierarchical'        => true,
    'public'              => true,
    'show_ui'             => true,//User interface
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'page',
       // 'rewrite'            => array( 'slug' => 'hotel-partners/now-resources-consultancies','with_front'  => false ), 
    'taxonomies' => array('post_tag') // this is IMPORTANT
  );
  register_post_type( 'casestudies', $args ); 
}
add_action( 'init', 'my_custom_post_casestudies');


function my_taxonomies_casestudies() { 
  $labels = array(
    'name'              => _x( 'Case Study Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Case Study Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Case Study Categories' ),
    'all_items'         => __( 'All Case Study Categories' ),
    'parent_item'       => __( 'Parent Case Study Category' ),
    'parent_item_colon' => __( 'Parent Case Study Category:' ),
    'edit_item'         => __( 'Edit Case Study Category' ), 
    'update_item'       => __( 'Update Case Study Category' ),
    'add_new_item'      => __( 'Add New Case Study Category' ),
    'new_item_name'     => __( 'New Case Study Category' ),
    'menu_name'         => __( 'Case Study Categories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
    'show_admin_column' => true,
    'query_var' => true, 
   //'rewrite'  => array( 'slug' => 'what-we-offer/now-resources/now-consultancies','with_front'  => false ),
  );
  register_taxonomy( 'case_study_category', 'casestudy', $args );
}
add_action( 'init', 'my_taxonomies_casestudies', 0 );

/*--assign single post tempaltes based upon category--*/
add_action('template_include', 'load_single_template');
  function load_single_template($template) {
    $new_template = '';

    // single post template
    if( is_single() ) {
      global $post;

      // 'wordpress' is category slugs
      if( has_term('wordpress', 'category', $post) ) {
        // use template file single-wordpress.php
        $new_template = locate_template(array('single-wordpress.php' ));
      }

    }
    return ('' != $new_template) ? $new_template : $template;
  }

/*--breadcrumbs--*/
function the_breadcrumb() {
  if (!is_home()) {
    echo '<a href="';
    echo get_option('home');
    echo '">';
    //bloginfo('name');
    echo "Home</a> / ";
    if (is_category() || is_single()) {
      //the_category('title_li=');
      if (is_single()) {
        //echo " / ";
        the_title();
      }
    } elseif (is_page()) {
      echo the_title();
    }
  }
}

//Custom image sizes
function wpdocs_setup_theme() {
add_theme_support( 'post-thumbnails' );
add_image_size( 'related-case-study', '482', '464', array( "center", "center") ); //Related case study thumb

}
add_action( 'after_setup_theme', 'wpdocs_setup_theme' );



?>
