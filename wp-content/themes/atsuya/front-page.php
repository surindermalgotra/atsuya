<?php
/**
 *  Template Name: Main Landing Page
    Created by: Sumesh Sharma
 */
get_header();
?>
<div class="lightbluebg" id="banner-area">
    <div class="container banner-container common-container-xl">
        <div class="row">
            <div class="col-sm-6">
                <h1>Tech-driven Operational Excellence for your Business and the Planet</h1>
                <p><a class="common-btn" href="javascript:void(0);">Find Your Industry</a></p>
            </div>
            <div class="col-sm-6 text-right position-relative">
                <div class="orbit orbit-right_top"></div>
                <img class="position-relative" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/lp-banner-image.png" alt="" title="" style="z-index:1" />
                <div class="orbit orbit-left_bottom"></div>
                <a class="common-btn demobtn" href="javascript:void(0);"><img src="<?php echo get_stylesheet_directory_uri()?>/assets/images/mail-all.png" alt="Free Demo" title="" />&nbsp; Get A Free Demo</a>
            </div>
        </div>
    </div>
</div>
<!--Our-Customers-->
<?php
    $partnerlogosGroup = get_field('our_customers_or_partners');
    if($partnerlogosGroup){
        $logosRepeater = $partnerlogosGroup['customers_or_partners_logos'];
 ?>
<div class="section-padding">
    <div class="container">
        <ul class="partners-section"><?php
            foreach($logosRepeater as $logo) {?>
                <li>
                    <img src="<?php echo $logo['upload_logo']['url'] ?>" alt="" title="" />
                </li>
              <?php }?>
        </ul>
    </div>
</div>
<?php } ?>
<!--/our-customers-->
<!--Our-Industries-section-->
<div class="section-padding" id="our-industreis-lp">
    <div class="orbit orbit-left_top noanimation"></div>
    <div class="container pad0">
        <div class="row">
            <div class="col-sm-4">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/our-industries-thumb.png" alt="Our Industries" title="" />
            </div>
            <div class="col-sm-8">
                <p class="subhead">Our Industries</p>
                <h2>We help industries make the best out of deep tech</h2>
                <p>Leverage trailblazing technology with full-stack solutions that accelerate your business’s operational efficiency and profits with an eye on sustainability.</p>
                <ul class="row">
                    <?php
                        $args = array(
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'category_name' => 'Industries',
                            'posts_per_page' => -1,
                        );
                        $arr_posts = new WP_Query( $args );
                          
                        if ( $arr_posts->have_posts() ) :
                          
                            while ( $arr_posts->have_posts() ) :
                                $arr_posts->the_post();
                                ?>
                                    <li class="col-sm-6 industries-links">
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </li>
                                <?php
                            endwhile;
                        endif;
                        wp_reset_query();
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--/Our-Industries-Section-->
<!--Monitor-Measure-Oprimize-->
<div class="lightbluebg section-padding text-center spacetb">
    <div class="container">
        <h2>Monitor, Measure and Optimize</h2>
        <p class="font-third-level mt-4">Use the power of data and analytics to make the smartest business decisions for efficiency and profitability.</p>
    </div>
</div>
<!--/Monitor-Measure-Optamize-->
<!--Business-Operations-->
<div class="custom-container-block" id="businessOperations">
    <div class='lightbluebg section-padding'>
        <h2>Business Operations<br /> at your Fingertips</h2>
        <p>Visualise and control every aspect of your business<br /> operations from your mobile and computer screens.</p>
        <p><a class="arrlnk" href="javascript:void(0);">Know more</a></p>
    </div>
    <div class="imgblock">
        <img class="right-positioned" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/image-business-operations.png" alt="" title="" />
    </div>
     <div class="orbit orbit-centered"></div>
</div>
<!--/Business-Operations-->
<!--How-We-Do-It-->
 <?php
    $HowWeDoItGroup = get_field('lp_how_we_do_it');
    if($HowWeDoItGroup){
        $sectionTitle = $HowWeDoItGroup['section_title'];
        $sectionThumb = $HowWeDoItGroup['how_we_do_it_image'];
        $hwdrepeater = $HowWeDoItGroup['how_we_do_list_points'];
    ?>
<div class="lightbluebg section-padding" id="howWeDo">
     <div class="orbit orbit-right_top noanimation"></div>
    <section class="container common-container-xl">
        <div class="row align-items-center">
            <div class="col-sm-8">
                <h2><?php echo $sectionTitle ?> </h2>
                <ul>
                    <?php 
                        foreach($hwdrepeater as $howWeDoPoints) {?>
                         <li>
                            <h3><span><img src="<?php echo $howWeDoPoints["hwd_list_point_icon"] ?>" alt="" title="" /></span> <?php echo $howWeDoPoints["hwd_list_point_title"] ?></h3>
                            <?php echo $howWeDoPoints["hwd_list_point_description"] ?>
                        </li>
                  <?php }?>
                </ul>
            </div>
            <div class="col-sm-4">
                <img src="<?php echo $sectionThumb['url'] ?>" alt="" title="" />
            </div>
        </div>
    </section>
</div>
<?php } ?>
<!--/How-We-Do-It-->
<!--Power-Tracebility-->
<div class="custom-container-block text-white" id="witness-power">
      <div class="orbit orbit-left_center"></div>
    <div class="imgblock">
        <img class="left-positioned" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/image-power-of-tracebility.png" alt="" title="" />
    </div>
    <div class='secondarybluebg section-padding'>
        <div class="block-content float-right">
            <h2>Witness the <br />Power of Traceability</h2>
            <p>From farm to fork, our solutions help you trace and track your product across the value chain.</p>
            <p><a class="arrlnk" href="javascript:void(0);">Know more</a></p>
        </div>
    </div>
    
</div>
<!--/end-power-tracebility-->
<!--Our-Solutions-Section-->
<div class="secondarybluebg section-padding" id="Our-Solutions-lp">
    <div class="orbit white orbit-right_top noanimation"></div>
    <div class="container common-container-xl">
        <div class="row">
            <div class="col-12">
                <p class="subhead">Our Solutions</p>
                <h2 class="text-white">We deliver value. See a limitless perspective of<br />what technology can do in your business domain.</h2>
            </div>
        </div>
         <div class="row mt-4">
             <?php
                $args = array(
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'category_name' => 'Solutions',
                    'posts_per_page' => 8,
                    'order' => 'ASC'
                );
                $arr_posts = new WP_Query( $args );
                  
                if ( $arr_posts->have_posts() ) :
                  
                    while ( $arr_posts->have_posts() ) :
                        $arr_posts->the_post();
                        ?>
                            <div class="col-sm-3 solutions-block">
                                <h4><?php the_title(); ?></h4>
                                <?php echo the_excerpt() ?>
                            </div>
                        <?php
                    endwhile;
                endif;
                wp_reset_query();
            ?>
         </div>
    </div>
    <div class="orbit white orbit-left_bottom noanimation"></div>
</div>
<!--/Our-Solutions-Section-->

<!--winners-banner-->
<div class="pad0 ad-section">
    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/winners-banner.jpg" alt="" title="" style="width:100%" />
</div>
<!--/end-winners-banner-->
<?php 
    $partnerlogos = get_field('lp_partner_logos');
    $partnerstitle = $partnerlogos['partners_section_title'];
    $mainheading =   $partnerlogos['partners_section_heading'];
    $partnerslogosRepeater = $partnerlogos['partners_logos'];
?>
<div class="container section-padding text-center" id="partners">
    <div class="spacetb">
        <p class="subhead"><?php echo $partnerstitle ?> </p>
        <h2><?php echo $mainheading ?></h2>
        <ul>
            <?php
                foreach($partnerslogosRepeater as $partnerslogo) { ?>
                    <li>
                        <img src="<?php echo $partnerslogo["partner_logo"]['url'] ?>" alt="" title="" /></span> 
                    </li>
            <?php }?>
        </ul>
    </div>
</div>
<?php
get_footer();
?>
