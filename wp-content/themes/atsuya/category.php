<?php
/**
 * The template for displaying archive pages
 * Template Name : Common Category
 *  Template Post Type: Post
 * Created by: Amit Kaushal
 */

get_header(); 
$current_category_id = get_query_var('cat');
?>

<div class="site-content-contain"></div><!-- Margin-div-->
<?php //$category_header_image = z_taxonomy_image_url($category_post_id, 'category-header-image'); ?>
<div class="col-xs-12 internal-banner cat-banner text-center nopad">
<?php 
	echo '<div class="banner-caption-internal"><h1 class="white text-uppercase nopad category-name">';?>
   <?php if($current_category_id == 5767){?> <span><?php single_cat_title(); ?></span> <?php } else {
        single_cat_title();
    }?>
	<?php echo '</h1>'; ?>
<?php echo '<p>'.category_description( $category_id ).'</p>' ?>  
	<?php echo '</div>'; ?>
		
</div>

<div class="clearfix"></div>
<div id="content" class="site-content container">
<div class="wrap listingwrap">
<div id="primary" class="content-area">
<main id="main" class="site-main" role="main">
<div class="breadcrumb-section bottom-bordered">
 <div class="col-xs-12 nopad"> 
     <?php 
        if($current_category_id == 5767){?>
            <div class="customcrumbs"><?php if (function_exists('qt_custom_breadcrumbs')) qt_custom_breadcrumbs();  ?></div>
        <?php } else {
            if (function_exists('qt_custom_breadcrumbs')) qt_custom_breadcrumbs(); 
        }?>
    </div>
</div>
<div class="clearfix"></div>
<div class="archive-pagead" style="max-width:1140px;margin-left:auto;margin-right:auto;">
<?php //wpads( 'FeaturesListingTop' ); ?> 
</div>
 
<div class="clearfix"></div>
<div class="row">
	<?php 

//Getting id of current category

$argscat = array('child_of' => $current_category_id);
	
//Getting child listing by parent Id if exist
	
$categories = get_categories( $argscat );

if(count($categories)>0){

	foreach($categories as $category) { 

		set_query_var( 'category', $category );
		get_template_part( 'template-parts/post/common-category/content', 'category-listing' );
		 
	}
	
} else {
        /*--only for eARTh category id (5767)--*/
       /*if($current_category_id == 5767){?>
            <h3 class="text-center" style="padding:10% 0">Coming Soon!<br />New articles starting Earth Day.</h3>
        <?php }*/
        /*--end condition for earth category--*/
		//Getting listing of post by parent category Id if there is no child category
		$paged1 = isset( $_GET['paged1'] ) ? (int) $_GET['paged1'] : 1;
		$postsPerPage = 12;
		$args = array(
					'post_type' => 'post', 
					'posts_per_page' => $postsPerPage,
                    'post__not_in' => array(3696,4071),/*--removed two posts from listing only--*/
					'cat' => $current_category_id, 
					'paged' => $paged1,
            'tax_query' => array( /*---Query to exclude the chinese posts from listing having post format as link--*/
                     array(
                     'taxonomy' => 'post_format',
                     'field' => 'slug',
                     'terms' => array('post-format-link'),
                     'operator' => 'NOT IN',
                     ),
                     )/*--- End exclusion of chinese posts--*/
				);
		$loop = new WP_Query($args);

		while ($loop->have_posts()) : $loop->the_post();
		
              get_template_part( 'template-parts/post/common-category/content', 'listing' );
             
		endwhile; // End of the loop.
		// Set the pagination args.
		$paginateArgs = array(
			'format'  => '?paged1=%#%',
			'current' => $paged1, // Reference the custom paged query we initially set.
			'total'   => $loop->max_num_pages // Max pages from our custom query.
		); ?>

		<!-- Pagination --->
		<div class="navigation nav-links">
			<?php echo paginate_links( $paginateArgs ); ?>
		</div> 
	
<?php } ?>
   </div>
    </main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->
</div>
<!--If category is concious Travel Tips-->
  <?php if($current_category_id == 3644){?>
        <div class="beforebooking whitebg">
            <div class="btnscontainer text-center">
                <h2>Tough questions to ask before you book</h2>
                <div class="qusetion-box-wrapper">
                    <a target="_blank" href="<?php site_url() ?>/downloads/docs/For-Pleasure-and-Business-Travellers-Tough-Qs-to-Ask-Before-Booking.pdf" class="text-center">Pleasure &amp; Business </a>
                    <a target="_blank" href="<?php site_url() ?>/downloads/docs/For-Sustainable-Meetings-and-Events-Tougher-Qs-to-Ask-Before-Booking.pdf" class="text-center">Meetings &amp; Events </a>
                    <a target="_blank" href="<?php site_url() ?>/downloads/docs/Wellbeing-How-Healthy-is-Your-Spa-Super-Tough-Qs-to-Ask-Before-Booking.pdf" class="text-center">
                        Wellbeing</a>
                </div>
            </div>
        </div>
   <?php } ?>
</div><!--#page-->
<div class="clearfix"></div>
<div class="footerspace"><?php get_footer(); ?> </div>

